// Loops
/* display Juan dela cruz on our console 10x*/
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");

/* display each element available on our array*/
let studentsA = ["TJ", "Mia", "Tin", "Chris"];

console.log(studentsA[0]);
console.log(studentsA[1]);
console.log(studentsA[2]);
console.log(studentsA[3]);

/*While Loop*/
let count = 5;//number of the iteration, number of times of how many we repeat our code

//while(/*condition - evaluates a given code if it's true or false. if the condition is true, the loop will start and continue the iteration or repetition of our block of code but if the condition is false the loop will stop*/){
	//block of code - this will be repeated by the loop
	//counter for iteration - the reason of continuous loop
//}

/* repeat a name Sylvan 5x*/
while(count !== 0){ //condition - if count value not equal to zero
	console.log("Sylvan");
	count--; //will be decremented by 1
}

/*print 1 to 5 using while loop*/
let number = 1;
while(number <= 5){
	console.log(number);
	number++;
}

//mini activity
// with a given array, print each element using while loop
let fruits = ['Banana', 'Mango'];
let indexNumber = 0; // reference to the index of our array
while(indexNumber <= 1){// the condition is based on the last index of elements that we have on an array
	console.log(fruits[indexNumber]);
	indexNumber++;
}

let mobilePhones = ['Samsung Galazy S21', 'Iphone 13 Pro', 'Xioami 11T', 'Realme C', 'Huawei Nova 8', 'Pixel 5', 'Asus Rog 6'];
console.log(mobilePhones.length);
console.log(mobilePhones.length -1);// will give us the last index position of element in an array
console.log(mobilePhones[mobilePhones.length - 1]);

let indexNumberForMobile = 0;
while(indexNumberForMobile <= mobilePhones.length-1){
	console.log(mobilePhones[indexNumberForMobile]);
	indexNumberForMobile++;
}

/*Do-While Loop - do the statement once, before going to the condition*/

let countA = 1;
do{
	console.log("Juan");
	countA++;
} while(countA <= 6);

let countB = 6;
 do{
 	console.log(`Do-While count ${countB}`);
 	countB--;
 } while(countB == 7);
console.log("=============Do-While vs While================");
 /*versus*/

 while(countB == 7){
 	console.log(`While count ${countB}`);
 	countB--;
 }

//mini activity
// with a given array, display each elements on the console using do-while
let indexNumberA = 0;
let computerBrands = ['Apple Macbook Pro', 'HP NoteBook', 'Asus', 'Lenovo', ' Acer', 'Dell', 'Huawei'];
do{
	console.log(computerBrands[indexNumberA]);
	indexNumberA++;
} while(indexNumberA <= computerBrands.length-1);

/*For Loop*/
	//variable - the scope of the declared variable is w/in the for loop
for(let count = 5; count >= 0; count--){
	console.log(count);
}

//mini activity
//given an array, kindly print each elemeent using for loop

let colors = ['Red', 'Green', 'Blue', 'Yellow', 'Purple', 'White', 'Black'];
for (let i = 0; i <= colors.length-1; i++) {
	console.log(colors[i]);
}

//continue and break
//break- stops the execution of our code
//continue- skip a block code and continue to the next iteration

/*
ages
	18, 19, 20, 21, 24, 25

	age == 21 (debutante age of boys), we will skip then go to the next iteration

	18, 19, 20, 24, 25
*/

let ages = [18, 19, 20, 21, 24, 25];
/*skip the debutante age of boys and girls using continue*/
for (let i = 0; i <= ages.length-1; i++) {
	if (ages[i] == 21 || ages[i] == 18) {
		continue;
	}
 	console.log(ages[i]);
}

/*
	let studentNames = ['Den', 'Jayson', 'Marvin', 'Rommel'];

	once we found Jayson on our array, we will stop the loop

	Den
	Jayson
*/
let studentNames = ['Den', 'Jayson', 'Marvin', 'Rommel'];
for (let i = 0; i <= studentNames.length-1; i++) {
	if (studentNames[i] == 'Jayson') {
		console.log(studentNames[i]);
		break;
	}	
}
/* Assignment*/
//Coding Challenge - Scope (Loops, Continue and Break)
// You can add the solution under our s16/d1/index.js only, no need to create a separate folder for this. Kindly push your solutions once you are done
//Then link your activity gitlab repo under Boodle WD078-16

/*
Instructions:

1. Given the array 'adultAge', display only adult age range on the console.

-- the goal of this activity is to exclude values that are not in the range of adult age. Using the tool loops, continue or break, 
the students must display only the given sample output below on their console.
*/
let adultAge = [20, 23, 33, 27, 18, 19, 70, 15, 55, 63, 85, 12, 19];
for (let i = 0; i <= adultAge.length-1; i++) {
	if(adultAge[i] < 20){
		continue;	
	}
	console.log(adultAge[i]);
}

/*
	Sample output:
  
  20
  23
  33
  27
  70
  55
  63
  85
*/

/*
Instructions:

2. Given an array 'students' and a function searchStudent, create a solution that, once a function is invoked with a given name of student as its argument,
	the function will start to loop and search for the student on a given array and once there's found it will print it on the console 
  and stop the execution of loop. 
  
  -- the goal of this activity is to print only the value needed based on the argument given. Use loop, and continue or break
*/
let students = ['Gary', 'Amelie', 'Anne', 'Jazz', 'Preina', 'James', 'Kelly', 'Diane', 'Lucy', 'Vanessa', 'Kim', 'Francine'];

function searchStudent(studentName){
	for(let i = 0; i <= students.length-1; i++){
		if(students[i] == "Jazz"){
			console.log(students[i]);
			break;
		}
	}
}

searchStudent('Jazz'); //invoked function with a given argument 'Jazz'

/*
	Sample output:
  
  Jazz
*/